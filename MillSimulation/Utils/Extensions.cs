﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Media;
using MillSimulation.Utils;
using OpenTK;

namespace PureCAD.Utils
{
    public static class Extensions
    {
        public static Vector3 ColorToVector3(this Color color)
        {
            return new Vector3(color.ScR, color.ScG, color.ScB);
        }

        public static T Clamp<T>(this T val, T min, T max) where T : IComparable<T>
        {
            if (val.CompareTo(min) < 0) return min;
            if (val.CompareTo(max) > 0) return max;
            return val;
        }

        public static Vector3 EulerAngles(this Quaternion q)
        {
            double heading, attitude, bank;
            double test = q.X * q.Y + q.Z * q.W;
            Vector3 euler = Vector3.Zero;

            if (test > 0.499)
            { // singularity at north pole
                heading = 2 * System.Math.Atan2(q.X, q.W);
                attitude = System.Math.PI / 2;
                bank = 0;
                euler = new Vector3((float)MathHelper.RadiansToDegrees(bank), (float)MathHelper.RadiansToDegrees(heading), (float)MathHelper.RadiansToDegrees(attitude));
                return euler;
            }
            if (test < -0.499)
            { // singularity at south pole
                heading = -2 * System.Math.Atan2(q.X, q.W);
                attitude = -System.Math.PI / 2;
                bank = 0;
                euler = new Vector3((float)MathHelper.RadiansToDegrees(bank), (float)MathHelper.RadiansToDegrees(heading), (float)MathHelper.RadiansToDegrees(attitude));
                return euler;
            }
            double sqx = q.X * q.X;
            double sqy = q.Y * q.Y;
            double sqz = q.Z * q.Z;
            heading = System.Math.Atan2(2 * q.Y * q.W - 2 * q.X * q.Z, 1 - 2 * sqy - 2 * sqz);
            attitude = System.Math.Asin(2 * test);
            bank = System.Math.Atan2(2 * q.X * q.W - 2 * q.Y * q.Z, 1 - 2 * sqx - 2 * sqz);
            euler = new Vector3((float)MathHelper.RadiansToDegrees(bank), (float)MathHelper.RadiansToDegrees(heading), (float)MathHelper.RadiansToDegrees(attitude));
            return euler;
        }

        public static string Truncate(this string str, int maxLength)
        {
            if (string.IsNullOrEmpty(str))
                return str;
            return str.Substring(0, Math.Min(str.Length, maxLength));
        }

        public static string SkipFirstN(this string str, int n, out string skipped)
        {
            if (string.IsNullOrEmpty(str))
            {
                skipped = string.Empty;
                return str;
            }
            if (n > str.Length)
            {
                skipped = str;
                return string.Empty;
            }
            skipped = string.Join(string.Empty, str.Take(n));
            return string.Join(string.Empty, str.Skip(n));
        }

        public static string SkipFirstN(this string str, int n)
        {
            if (string.IsNullOrEmpty(str))
            {
                return str;
            }
            if (n > str.Length)
            {
                return string.Empty;
            }
            return string.Join(string.Empty, str.Skip(n));
        }

        public static string GetAfterOrEmpty(this string text, string stopAt = "-", bool skip = false)
        {
            if (!String.IsNullOrWhiteSpace(text))
            {
                int charLocation = text.IndexOf(stopAt, StringComparison.Ordinal);

                if (charLocation >= 0)
                {
                    if (skip)
                    {
                        charLocation = text.IndexOf(stopAt, StringComparison.Ordinal) + stopAt.Length;
                        return text.Substring(charLocation, text.Length - charLocation);
                    }
                    return text.Substring(charLocation, text.Length - charLocation);
                }
            }

            return String.Empty;
        }

        public static string GetBeforeOrEmpty(this string text, string stopAt = "-")
        {
            if (!String.IsNullOrWhiteSpace(text))
            {
                int charLocation = text.IndexOf(stopAt, StringComparison.Ordinal);

                if (charLocation > 0)
                {
                    return text.Substring(0, charLocation);
                }
            }

            return String.Empty;
        }

        public static string ZeroIfEmpty(this string text)
        {
            if (string.IsNullOrWhiteSpace(text))
                return "0";
            return text;
        }

        public static Vector3 ParseToVector3(this string text, out bool parseResult)
        {
            float x = float.PositiveInfinity, y = float.PositiveInfinity, z = float.PositiveInfinity;
            var strX = text.GetAfterOrEmpty("X", true).GetBeforeOrEmpty("Y").ZeroIfEmpty();
            var strY = text.GetAfterOrEmpty("Y", true).GetBeforeOrEmpty("Z").ZeroIfEmpty();
            var strZ = text.GetAfterOrEmpty("Z", true).ZeroIfEmpty();
            parseResult = float.TryParse(strX, NumberStyles.Any, CultureInfo.InvariantCulture, out x) &&
                          float.TryParse(strY, NumberStyles.Any, CultureInfo.InvariantCulture, out y) &&
                          float.TryParse(strZ, NumberStyles.Any, CultureInfo.InvariantCulture, out z);
            return new Vector3(x, y, z);
        }

        public static Vector3 ConvertFileVector(this Vector3 vec)
        {
            return new Vector3(vec.X, vec.Z, -vec.Y);
        }
    }
}
