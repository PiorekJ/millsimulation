﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MillSimulation.Models;
using MillSimulation.Utils.Storage;
using OpenTK;
using PureCAD.Utils;

namespace MillSimulation.Utils
{
    public static class FileManager
    {
        public static SceneMillPath ReadPathFile(string path)
        {
            var fileExt = Path.GetExtension(path).SkipFirstN(1);
            var fileMillType = fileExt.Truncate(1);
            var millSize = -1;
            var millType = MillType.Undefined;
            if (!int.TryParse(fileExt.SkipFirstN(1), out millSize))
            {
                MessageBox.Show("Incorrect file extension.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return null;
            }
            try
            {
                switch (fileMillType)
                {
                    case "k":
                        millType = MillType.Sphere;
                        break;
                    case "f":
                        millType = MillType.Flat;
                        break;
                    default:
                        MessageBox.Show("Incorrect file extension.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return null;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
            var nodes = GetPathNodes(path);
            if (nodes != null)
                return new SceneMillPath(Path.GetFileName(path), millType, (float)(millSize * Constants.FileScaleFactor) / 2.0f, nodes);
            return null;
        }

        private static List<Vector3> GetPathNodes(string filePath)
        {
            List<Vector3> list = new List<Vector3>();
            var file = new StreamReader(filePath);
            string line;
            var commandNumber = -1;
            var lineNumber = 0;
            while ((line = file.ReadLine()) != null)
            {
                if (line.Length > 0)
                {
                    if (line[0] == 'N')
                    {
                        var stripline = line.GetAfterOrEmpty("N", true);

                        if (!stripline.GetBeforeOrEmpty("G").Equals(String.Empty))
                        {
                            var result = -1;
                            if (int.TryParse(stripline.GetBeforeOrEmpty("G"), out result))
                            {
                                commandNumber = result;
                                break;
                            }
                            else
                            {
                                OpenErrorMessageBox($"Undefined line number in {line}");
                                return null;
                            }
                        }
                        else if(!stripline.GetBeforeOrEmpty("S").Equals(String.Empty))
                        {
                            var result = -1;
                            if (int.TryParse(stripline.GetBeforeOrEmpty("S"), out result))
                            {
                                commandNumber = result;
                                break;
                            }
                            else
                            {
                                OpenErrorMessageBox($"Undefined line number in {line}");
                                return null;
                            }
                        }
                        else if (!stripline.GetBeforeOrEmpty("F").Equals(String.Empty))
                        {
                            var result = -1;
                            if (int.TryParse(stripline.GetBeforeOrEmpty("F"), out result))
                            {
                                commandNumber = result;
                                break;
                            }
                            else
                            {
                                OpenErrorMessageBox($"Undefined line number in {line}");
                                return null;
                            }
                        }
                        else
                        {
                            OpenErrorMessageBox($"Couldn't locate starting command number in the file");
                            return null;
                        }
                    }
                }
            }
            file.Close();
            file = new StreamReader(filePath);
            while ((line = file.ReadLine()) != null)
            {
                if (!ReadLine(line, list, ref commandNumber, ref lineNumber))
                {
                    file.Close();
                    return null;
                }
                lineNumber++;
            }

            file.Close();
            return list;
        }

        private static bool ReadLine(string line, List<Vector3> nodeList, ref int commandNumber, ref int lineNumber)
        {
            if (line.Length > 0)
            {
                switch (line[0])
                {
                    case '%':
                        return true;
                    case 'N':
                        var stripline = line.GetAfterOrEmpty("N" + commandNumber, true);
                        var helper = string.Empty;
                        if (stripline.Equals("G40G90"))
                        {
                            commandNumber++;
                            return true;
                        }
                        else if (!(stripline.GetAfterOrEmpty("S", true)).Equals(String.Empty))
                        {
                            if (!line.GetAfterOrEmpty("N", true).GetBeforeOrEmpty("S").Equals(commandNumber.ToString()))
                            {
                                OpenErrorMessageBox(
                                    $"Error in line: {lineNumber}, incorrect command number, expected {commandNumber}");
                                return false;
                            }
                            stripline = stripline.GetAfterOrEmpty("S", true);
                            if (!(helper = stripline.GetAfterOrEmpty("M", true)).Equals(String.Empty))
                            {
                                var result = -1;
                                if (int.TryParse(stripline.GetBeforeOrEmpty("M"), out result))
                                {
                                    Console.WriteLine($"Mill rotation speed: {result}");
                                }
                                else
                                {
                                    OpenErrorMessageBox(
                                        $"Error in line: {lineNumber} expected mill rotation speed, got {stripline.GetBeforeOrEmpty("M")}");
                                    return false;
                                }

                                if (helper.Equals("03"))
                                {
                                    commandNumber++;
                                    return true;
                                }
                                OpenErrorMessageBox($"Error in line: {lineNumber} expected M03, got {stripline}");
                                return false;
                            }
                        }
                        else if (!(stripline.GetAfterOrEmpty("F", true)).Equals(String.Empty))
                        {
                            if (!line.GetAfterOrEmpty("N", true).GetBeforeOrEmpty("F").Equals(commandNumber.ToString()))
                            {
                                OpenErrorMessageBox(
                                    $"Error in line: {lineNumber}, incorrect command number, expected {commandNumber}");
                                return false;
                            }
                            stripline = stripline.GetAfterOrEmpty("F", true);
                            var result = -1;
                            if (int.TryParse(stripline, out result))
                            {
                                Console.WriteLine($"Mill move speed: {result}");
                                commandNumber++;
                                return true;
                            }
                            OpenErrorMessageBox(
                                $"Error in line: {lineNumber} expected mill move speed, got {stripline}");
                            return false;

                        }
                        else if (!(line.GetAfterOrEmpty("X")).Equals(String.Empty))
                        {
                            if (!line.GetAfterOrEmpty("N", true).GetBeforeOrEmpty("G").Equals(commandNumber.ToString()))
                            {
                                OpenErrorMessageBox(
                                    $"Error in line: {lineNumber}, incorrect command number, expected {commandNumber}");
                                return false;
                            }
                            stripline = line.GetAfterOrEmpty("X");
                            var vec = stripline.ParseToVector3(out var parseResult);
                            if (parseResult)
                            {
                                nodeList.Add(vec.ConvertFileVector() * Constants.FileScaleFactor);
                                commandNumber++;
                                return true;
                            }
                            OpenErrorMessageBox($"Couldn't parse scene node instruction {lineNumber}");
                            return false;
                        }
                        OpenErrorMessageBox($"Undefined line sequence in line {lineNumber}: {line}");
                        return false;
                    default:
                        OpenErrorMessageBox($"Undefined line type sequence in line {lineNumber}, found: {line[0]}");
                        return false;
                }
            }
            return false;
        }

        private static void OpenErrorMessageBox(string text)
        {
            MessageBox.Show(text, "File parse error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
