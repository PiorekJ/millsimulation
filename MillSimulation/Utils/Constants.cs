﻿using System.Windows.Media;
using OpenTK;
using static System.Windows.Media.ColorConverter;

namespace MillSimulation.Utils
{
    public static class Constants
    {
        public const int LineSize = 2;

        public static Vector3 DefaultCameraPosition = new Vector3(0, 12, 15);
        public static float CameraRotationXDeg = -30;
        public static float CameraRotationYDeg = 0;
        public static Quaternion DefaultCameraRotation = new Quaternion(MathHelper.DegreesToRadians(CameraRotationXDeg), MathHelper.DegreesToRadians(CameraRotationYDeg), 0);
        public const float DefaultFOV = (float)System.Math.PI / 4;
        public const float DefaultZNear = 0.01f;
        public const float DefaultZFar = 150f;

        public const float CameraMovementMouseSensitivity = 0.5f;
        public const float CameraRotationMouseSensitivity = 0.05f;
        public const float CameraZoomMouseSensitivity = 0.5f;

        public const float CameraMovementKeyVelocity = 5.0f;
        public const float CameraMovementKeySlowVelocity = 0.25f;

        public static readonly Color BackgroundColor = (Color)ConvertFromString("#FF616161");
        public const float FileScaleFactor = 0.1f;
        public static float Epsilon = 0.000001f;
        public static readonly Vector3 SafeMillPosition = new Vector3(0,15,0);
    }
}
