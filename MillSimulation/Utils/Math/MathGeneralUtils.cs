﻿using System;
using System.Collections.Generic;
using OpenTK;

namespace MillSimulation.Utils.Math
{
    public static class MathGeneralUtils
    {
        public static Matrix4 CreatePerspectiveFOVMatrix(float fov, float aspectRatio, float zNear, float zFar)
        {

            if (fov <= 0 || fov > System.Math.PI)
                throw new ArgumentOutOfRangeException("Incorrect fov");
            if (aspectRatio <= 0)
                throw new ArgumentOutOfRangeException("Incorrect aspect ratio");
            if (zNear <= 0)
                throw new ArgumentOutOfRangeException("Incorrect zNear");
            if (zFar <= 0)
                throw new ArgumentOutOfRangeException("Incorrect zFar");
            if (zNear >= zFar)
                throw new ArgumentOutOfRangeException("Incorrect zNear");

            float yMax = zNear * (float)System.Math.Tan(0.5f * fov);
            float yMin = -yMax;
            float xMin = yMin * aspectRatio;
            float xMax = yMax * aspectRatio;

            if (zNear <= 0)
                throw new ArgumentOutOfRangeException("zNear");
            if (zFar <= 0)
                throw new ArgumentOutOfRangeException("zFar");
            if (zNear >= zFar)
                throw new ArgumentOutOfRangeException("zNear");

            float x = (2.0f * zNear) / (xMax - xMin);
            float y = (2.0f * zNear) / (yMax - yMin);
            float a = (xMax + xMin) / (xMax - xMin);
            float b = (yMax + yMin) / (yMax - yMin);
            float c = -(zFar + zNear) / (zFar - zNear);
            float d = -(2.0f * zFar * zNear) / (zFar - zNear);

            return new Matrix4(x, 0, 0, 0,
                0, y, 0, 0,
                a, b, c, -1,
                0, 0, d, 0);
        }

        public static Matrix4 CreateTranslation(Vector3 position)
        {
            Matrix4 result = Matrix4.Identity;
            result.Row3 = new Vector4(position.X, position.Y, position.Z, 1.0f);
            return result;
        }

        public static Matrix4 CreateFromQuaternion(Quaternion rotation)
        {
            rotation.ToAxisAngle(out var axis, out var angle);

            float cos = (float)System.Math.Cos(-angle);
            float sin = (float)System.Math.Sin(-angle);
            float t = 1.0f - cos;

            axis.Normalize();

            Matrix4 result = Matrix4.Identity;
            result.Row0 = new Vector4(t * axis.X * axis.X + cos, t * axis.X * axis.Y - sin * axis.Z, t * axis.X * axis.Z + sin * axis.Y, 0.0f);
            result.Row1 = new Vector4(t * axis.X * axis.Y + sin * axis.Z, t * axis.Y * axis.Y + cos, t * axis.Y * axis.Z - sin * axis.X, 0.0f);
            result.Row2 = new Vector4(t * axis.X * axis.Z - sin * axis.Y, t * axis.Y * axis.Z + sin * axis.X, t * axis.Z * axis.Z + cos, 0.0f);
            result.Row3 = Vector4.UnitW;
            return result;
        }

        public static Matrix4 CreateScale(Vector3 scale)
        {
            Matrix4 result = Matrix4.Identity;
            result.Row0.X = scale.X;
            result.Row1.Y = scale.Y;
            result.Row2.Z = scale.Z;
            return result;
        }

        public static List<Vector3> Bresenham3D(int x1, int y1, int z1, int x2, int y2, int z2)
        {
            var retEnumerable = new List<Vector3>();
            int xd, yd, zd;
            int x, y, z;
            int ax, ay, az;
            int sx, sy, sz;
            int dx, dy, dz;

            dx = x2 - x1;
            dy = y2 - y1;
            dz = z2 - z1;

            ax = System.Math.Abs(dx) * 2;
            ay = System.Math.Abs(dy) * 2;
            az = System.Math.Abs(dz) * 2;

            sx = System.Math.Sign(dx);
            sy = System.Math.Sign(dy);
            sz = System.Math.Sign(dz);

            x = x1;
            y = y1;
            z = z1;

            if (ax >= System.Math.Max(ay, az))
            {
                yd = ay - ax / 2;
                zd = az - ax / 2;

                while (true)
                {
                    retEnumerable.Add(new Vector3(x, y, z));

                    if (x == x2)
                        break;

                    if (yd >= 0)
                    {
                        y += sy;
                        yd -= ax;
                    }
                    if (zd >= 0)
                    {
                        z += sz;
                        zd -= ax;
                    }

                    x += sx;
                    yd += ay;
                    zd += az;
                }

            }
            else if (ay >= System.Math.Max(ax, az))
            {
                xd = ax - ay / 2;
                zd = az - ay / 2;
                while (true)
                {
                    retEnumerable.Add(new Vector3(x, y, z));

                    if (y == y2)
                        break;

                    if (xd >= 0)
                    {
                        x += sx;
                        xd -= ay;
                    }

                    if (zd >= 0)
                    {
                        z += sz;
                        zd -= ay;
                    }

                    y += sy;
                    xd += ax;
                    zd += az;
                }
            }
            else if (az >= System.Math.Max(ax, ay))
            {
                xd = ax - az / 2;
                yd = ay - az / 2;
                while (true)
                {
                    retEnumerable.Add(new Vector3(x, y, z));

                    if (z == z2)
                        break;

                    if (xd >= 0)
                    {
                        x += sx;
                        xd -= az;
                    }

                    if (yd >= 0)
                    {
                        y += sy;
                        yd -= az;
                    }

                    z += sz;
                    xd += ax;
                    yd += ay;
                }
            }

            return retEnumerable;
        }

        public static List<Vector2> Bresenham2D(int x1, int y1, int x2, int y2)
        {
            var retEnumerable = new List<Vector2>();
            int w = x2 - x1;
            int h = y2 - y1;
            int dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0;
            if (w < 0) dx1 = -1; else if (w > 0) dx1 = 1;
            if (h < 0) dy1 = -1; else if (h > 0) dy1 = 1;
            if (w < 0) dx2 = -1; else if (w > 0) dx2 = 1;
            int longest = System.Math.Abs(w);
            int shortest = System.Math.Abs(h);
            if (!(longest > shortest))
            {
                longest = System.Math.Abs(h);
                shortest = System.Math.Abs(w);
                if (h < 0) dy2 = -1; else if (h > 0) dy2 = 1;
                dx2 = 0;
            }
            int numerator = longest >> 1;
            for (int i = 0; i <= longest; i++)
            {
                retEnumerable.Add(new Vector2(x1, y1));
                numerator += shortest;
                if (!(numerator < longest))
                {
                    numerator -= longest;
                    x1 += dx1;
                    y1 += dy1;
                }
                else
                {
                    x1 += dx2;
                    y1 += dy2;
                }
            }
            return retEnumerable;
        }

        public static Vector3 MoveTowards(Vector3 current, Vector3 target, float maxDistanceDelta)
        {
            Vector3 dir = target - current;
            float magnitude = dir.Length;
            if (magnitude <= maxDistanceDelta || System.Math.Abs(magnitude) < Constants.Epsilon)
            {
                return target;
            }
            return current + dir / magnitude * maxDistanceDelta;
        }

        public static Vector3 Lerp(Vector3 from, Vector3 to, float t)
        {
            return (1 - t) * from + t * to;
        }

    }
}
