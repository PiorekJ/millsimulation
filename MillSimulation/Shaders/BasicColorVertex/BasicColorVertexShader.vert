﻿#version 330 core

in layout(location = 0) vec3 position;
in layout(location = 1) vec3 color;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

mat4 projectionViewModel = projection*view*model;

out VS_OUT 
{
	vec3 color;
} Out;

void main()
{
	gl_Position = projectionViewModel * vec4(position,1);
	Out.color = color;
}
