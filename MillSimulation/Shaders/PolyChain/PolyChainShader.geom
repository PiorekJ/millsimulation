﻿#version 330 core

layout(lines) in;
layout(line_strip, max_vertices = 2) out;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

mat4 projectionViewModel = projection * view * model;

in V_Out
{
	vec3 color;
} In[];

out GEO_Out
{
	vec3 color;
} Out;


void main()
{
	gl_Position = projectionViewModel * gl_in[0].gl_Position;
	Out.color = In[0].color;
	EmitVertex();
	gl_Position = projectionViewModel * gl_in[1].gl_Position;
	Out.color = In[1].color;
	EmitVertex();
	EndPrimitive();
}