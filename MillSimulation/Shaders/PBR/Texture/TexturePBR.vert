﻿#version 330 core

in layout (location = 0) vec3 inPosition;
in layout (location = 1) vec3 inNormal;
in layout (location = 2) vec2 inTexCoords;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

out vec2 TexCoords;
out vec3 WorldPos;
out vec3 Normal;

mat4 projectionView = projection*view;

void main()
{
	TexCoords = inTexCoords;
	Normal = mat3(model) * inNormal; 
	WorldPos = vec3(model * vec4(inPosition, 1.0));
	gl_Position = projectionView * vec4(WorldPos,1);
}
