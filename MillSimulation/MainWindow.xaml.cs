﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using MillSimulation.Core;
using MillSimulation.Utils;
using OpenTK.Graphics.OpenGL;
using InputManager = MillSimulation.Core.InputManager;

namespace MillSimulation
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Simulation Simulation { get; set; }


        public MainWindow()
        {
            Simulation = new Simulation();
            InitializeComponent();
            //var list = FileManager.ReadPathFile("./Paths/Violin/t1.k16", out var millSize, out var millType);
        }

        private void DisplayControl_OnControlLoaded(object sender, RoutedEventArgs e)
        {
            Simulation.Scene.Camera.SetViewportValues(DisplayControl.AspectRatio, DisplayControl.ControlWidth, DisplayControl.ControlHeigth);
            GL.Viewport(0, 0, DisplayControl.ControlWidth, DisplayControl.ControlHeigth);
            GL.Enable(EnableCap.DepthTest);

            Simulation.InitializeSimulation();

            CompositionTarget.Rendering += OnRender;
        }

        private void OnRender(object sender, EventArgs e)
        {
            Simulation.TimeCounter.CountDT();

            GL.ClearColor(Constants.BackgroundColor.ScR, Constants.BackgroundColor.ScG, Constants.BackgroundColor.ScB, Constants.BackgroundColor.ScA);
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            Simulation.Scene.Camera.UpdateCamera(DisplayControl.MousePosition);

            Simulation.Scene.DrawScene();

            DisplayControl.SwapBuffers();
        }



        private void DisplayControl_OnControlResized(object sender, RoutedEventArgs e)
        {
            Simulation.Scene.Camera.SetViewportValues(DisplayControl.AspectRatio, DisplayControl.ControlWidth, DisplayControl.ControlHeigth);
            GL.Viewport(0, 0, DisplayControl.ControlWidth, DisplayControl.ControlHeigth);
        }

        private void DisplayControl_OnControlUnloaded(object sender, RoutedEventArgs e)
        {
            foreach (SceneObject item in Simulation.Scene.SceneObjects)
            {
                item.Dispose();
            }
        }

        private void DisplayControl_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            Simulation.Scene.Camera.SetLastMousePosition(DisplayControl.MousePosition);
            InputManager.OnMouseButtonChange(e.ChangedButton, true);
        }

        private void DisplayControl_OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            InputManager.OnMouseButtonChange(e.ChangedButton, false);
        }

        private void DisplayControl_OnKeyDown(object sender, KeyEventArgs e)
        {
            InputManager.OnKeyChange(e.Key, true);
        }

        private void DisplayControl_OnKeyUp(object sender, KeyEventArgs e)
        {
            InputManager.OnKeyChange(e.Key, false);
        }

        private void DisplayControl_OnMouseWheel(object sender, MouseWheelEventArgs e)
        {
            InputManager.OnMouseScroll(e.Delta);
        }
    }
}
