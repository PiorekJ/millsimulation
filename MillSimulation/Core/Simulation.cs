﻿using System;
using System.Collections.ObjectModel;
using MillSimulation.Models;
using MillSimulation.Utils;
using MillSimulation.Utils.Storage;
using OpenTK;

namespace MillSimulation.Core
{
    public class Simulation
    {
        public static Scene Scene { get; set; }
        public static UIManager UIManager { get; set; }
        public static Settings Settings { get; set; }

        public TimeCounter TimeCounter;

        public static float DeltaTime => TimeCounter.DeltaTime;

        public Simulation()
        {
            Scene = new Scene();
            Settings = new Settings();
            UIManager = new UIManager();
            TimeCounter = new TimeCounter();
        }

        public void InitializeSimulation()
        {
            TimeCounter.Start();
            Scene.AddSceneBackground();
            Scene.AddSceneCursor();
            Scene.AddSceneLightAt(new Vector3(0, 10, 0));
            Scene.AddSceneLightAt(new Vector3(-5, 10, 5));
            Scene.AddSceneLightAt(new Vector3(5, 10, -5));
            Scene.AddSceneLightAt(new Vector3(5, 10, 5));
            Scene.AddSceneLightAt(new Vector3(-5, 10, -5));
            //Scene.AddSceneSphere();
            Scene.AddSceneMillFlat().IsActive = false;
            Scene.AddSceneMillSphere().IsActive = false;
            Scene.AddSceneMillingMachine(Scene.AddSceneMaterial());
        }
    }
}
