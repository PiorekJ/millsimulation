﻿using System;
using System.Collections.Generic;
using MillSimulation.Components;
using MillSimulation.Utils.UIExtensionClasses;

namespace MillSimulation.Core
{
    public interface ISelectable
    {
        bool IsSelected { get; set; }
    }



    public class SceneObject : BindableObject, IDisposable
    {

        private bool _isActive = true;
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                RaisePropertyChanged();
            }
        }

        public Transform Transform { get; set; }

        private List<Component> _components;

        public delegate void DeleteHandler();

        public event DeleteHandler OnDeleteEvent;

        #region UI Related

        public bool IsVisibleOnList
        {
            get { return _isVisibleOnList; }
            set
            {
                _isVisibleOnList = value;
                RaisePropertyChanged();
            }
        }
        private bool _isVisibleOnList = true;

        public string ObjectName
        {
            get => _objectName;
            set
            {
                _objectName = value;
                RaisePropertyChanged();
            }
        }

        private string _objectName;

        #endregion

        public SceneObject()
        {
            _components = new List<Component>();
            Transform = AddComponent<Transform>();
        }

        public TComponent AddComponent<TComponent>()
            where TComponent : Component, new()
        {
            TComponent component = new TComponent { Owner = this };
            _components.Add(component);
            return component;
        }

        public TComponent GetComponent<TComponent>()
            where TComponent : Component
        {
            foreach (var item in _components)
            {
                if (item is TComponent)
                {
                    return (TComponent)item;
                }
            }
            return null;
        }

        public void RemoveComponent(Component component)
        {
            component.Dispose();
            _components.Remove(component);
        }

        protected virtual void OnUpdate()
        {

        }
        
        public virtual void Update()
        {
            if (!IsActive)
                return;
            OnUpdate();
        }

        public void OnDelete()
        {
            OnDeleteEvent?.Invoke();
        }

        public virtual void Dispose()
        {
            for (var i = _components.Count - 1; i >= 0; i--)
            {
                Component component = _components[i];
                RemoveComponent(component);
            }
        }

    }
}
