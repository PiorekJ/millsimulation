﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using MillSimulation.Models;
using MillSimulation.Utils.UIExtensionClasses;
using OpenTK;

namespace MillSimulation.Core
{
    public class Scene : BindableObject
    {
        private SceneMillingMachine _sceneMillingMachine;
        private SceneMaterial _sceneMaterial;
        public Camera Camera { get; set; }
        public SceneCursor SceneCursor { get; set; }
        public SceneBackground SceneBackground { get; set; }
        public List<SceneLight> SceneLights { get; set; }
        public SceneMillFlat SceneMillFlat { get; set; }
        public SceneMillSphere SceneMillSphere { get; set; }

        public SceneMaterial SceneMaterial
        {
            get { return _sceneMaterial; }
            set
            {
                _sceneMaterial = value; 
                RaisePropertyChanged();
            }
        }

        public SceneMillingMachine SceneMillingMachine
        {
            get { return _sceneMillingMachine; }
            set
            {
                _sceneMillingMachine = value;
                RaisePropertyChanged();
            }
        }

        public ObservableCollection<SceneObject> SceneObjects { get; set; }

        #region Commands


        #endregion

        public Scene()
        {
            Camera = new Camera();
            SceneLights = new List<SceneLight>();
            SceneObjects = new ObservableCollection<SceneObject>();
        }


        public SceneCursor AddSceneCursor()
        {
            SceneCursor = new SceneCursor();
            return SceneCursor;
        }

        public SceneBackground AddSceneBackground()
        {
            SceneBackground = new SceneBackground();
            return SceneBackground;
        }

        public SceneLight AddSceneLightAt(Vector3 position)
        {
            var item = new SceneLight();
            item.Transform.Position = position;
            SceneLights.Add(item);
            SceneObjects.Add(item);
            return item;
        }

        public SceneMillFlat AddSceneMillFlat()
        {
            SceneMillFlat = new SceneMillFlat();
            SceneObjects.Add(SceneMillFlat);
            return SceneMillFlat;
        }

        public SceneMillSphere AddSceneMillSphere()
        {
            SceneMillSphere = new SceneMillSphere();
            SceneObjects.Add(SceneMillSphere);
            return SceneMillSphere;
        }

        public SceneMillingMachine AddSceneMillingMachine(SceneMaterial material)
        {
            SceneMillingMachine = new SceneMillingMachine(material);
            SceneObjects.Add(SceneMillingMachine);
            return SceneMillingMachine;
        }

        public SceneMaterial AddSceneMaterial()
        {
            SceneMaterial = new SceneMaterial();
            SceneObjects.Add(SceneMaterial);
            return SceneMaterial;
        }

        public SceneSphere AddSceneSphere()
        {
            var item = new SceneSphere();
            SceneObjects.Add(item);
            return item;
        }

        public SceneMillPath AddSceneMillPath(SceneMillPath path)
        {
            SceneObjects.Add(path);
            return path;
        }

        public void DrawScene()
        {
            SceneBackground.Update();
            SceneBackground.Render();
            SceneCursor.Update();
            SceneCursor.Render();

            for (var i = SceneObjects.Count - 1; i >= 0; i--)
            {
                SceneObject item = Simulation.Scene.SceneObjects[i];
                item.Update();
                if (item is Model model)
                    model.Render();
            }
        }

        public void RemoveSceneObject(SceneObject item)
        {
            if (item != null)
            {
                SceneObjects.Remove(item);
                item.Dispose();
            }
        }




    }
}