﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using MillSimulation.Models;
using MillSimulation.Utils;
using MillSimulation.Utils.Storage;
using MillSimulation.Utils.UIExtensionClasses;

namespace MillSimulation.Core
{
    public class UIManager : BindableObject
    {
        private SceneMillPath _selectedPath;

        public SceneMillPath SelectedPath
        {
            get { return _selectedPath; }
            set
            {
                _selectedPath = value; 
                RaisePropertyChanged();
            }
        }

        public ObservableCollection<SceneMillPath> Paths { get; set; }

        #region Commands

        public ICommand OpenPathCommand { get; set; }
        public ICommand MillSelectedCommand { get; set; }
        public ICommand ToggleSimulationCommand { get; set; }
        public ICommand ResetSimulationCommand { get; set; }
        public ICommand FastForwardCommand { get; set; }
        #endregion

        public UIManager()
        {
            Paths = new ObservableCollection<SceneMillPath>();
            OpenPathCommand = new RelayCommand(param => OpenPath());
            MillSelectedCommand = new RelayCommand(param => MillSelected());
            ToggleSimulationCommand = new RelayCommand(param => ToggleSimulation());
            ResetSimulationCommand = new RelayCommand(param => ResetSimulation());
            FastForwardCommand = new RelayCommand(param => FastForwardSimulation());
        }

        

        private void ResetSimulation()
        {
            foreach (SceneMillPath path in Paths)
            {
                path.CurrentSceneIndex = 0;
                foreach (SceneMillPath.ScenePath scenePath in path.ScenePaths)
                {
                    scenePath.CurrentMaterialIndex = 0;
                }
            }
            Simulation.Scene.SceneMaterial.RegenerateMesh();
            Simulation.Scene.SceneMillingMachine.IsRunning = false;
            Simulation.Scene.SceneMillingMachine.ClearPaths();
            Simulation.Scene.SceneMillFlat.IsActive = false;
            Simulation.Scene.SceneMillSphere.IsActive = false;
        }

        private void ToggleSimulation()
        {
            if (Simulation.Scene.SceneMillingMachine != null)
                Simulation.Scene.SceneMillingMachine.IsRunning = !Simulation.Scene.SceneMillingMachine.IsRunning;
        }

        private void OpenPath()
        {
            OpenFileDialog openDialog = new OpenFileDialog { Multiselect = true };
            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                foreach (string fileName in openDialog.FileNames)
                {
                    var path = FileManager.ReadPathFile(fileName);
                    if (path != null)
                    {
                        Paths.Add(path);
                        Simulation.Scene.AddSceneMillPath(path);
                        path.SetupScenePaths(Simulation.Scene.SceneMaterial, path.MillSize);
                    }
                }


            }
        }

        private void FastForwardSimulation()
        {
            foreach (SceneMillPath path in Paths)
            {
                path.SetupScenePaths(Simulation.Scene.SceneMaterial, path.MillSize);
                Simulation.Scene.SceneMillingMachine.MillInstant(path);
            }
        }

        public void DeletePath(SceneMillPath path)
        {
            Paths.Remove(path);
            Simulation.Scene.SceneMillingMachine.DeletePath(path);
        }

        private void MillSelected()
        {
            if (!Paths.Any(x => x.IsSelected))
                return;
            Simulation.Scene.SceneMillingMachine.ClearPaths();
            foreach (SceneMillPath path in Paths)
            {
                if (path.IsSelected)
                {
                    path.CurrentSceneIndex = 0;
                    path.SetupScenePaths(Simulation.Scene.SceneMaterial, path.MillSize);
                    foreach (SceneMillPath.ScenePath scenePath in path.ScenePaths)
                    {
                        scenePath.CurrentMaterialIndex = 0;
                    }
                    Simulation.Scene.SceneMillingMachine.AddPath(path);
                }
            }
            Simulation.Scene.SceneMillingMachine.StartMilling(Simulation.Scene.SceneMillingMachine.Paths[0]);
        }
    }
}
