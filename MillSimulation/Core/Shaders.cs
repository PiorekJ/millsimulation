﻿using MillSimulation.OpenTK;

namespace MillSimulation.Core
{
    public static class Shaders
    {
        private static Shader _basicShader;
        public static Shader BasicShader
        {
            get
            {
                if (_basicShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/Basic/BasicShader.vert"),
                        frag = new FragmentShaderObject("./Shaders/Basic/BasicShader.frag"))
                    {
                        _basicShader = new Shader(vert, frag);
                    }
                    return _basicShader;
                }
                return _basicShader;
            }
        }

        private static Shader _basicColorShader;
        public static Shader BasicColorShader
        {
            get
            {
                if (_basicColorShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/BasicColor/BasicColorShader.vert"),
                        frag = new FragmentShaderObject("./Shaders/BasicColor/BasicColorShader.frag"))
                    {
                        _basicColorShader = new Shader(vert, frag);
                    }
                    return _basicColorShader;
                }
                return _basicColorShader;
            }
        }

        private static Shader _basicColorVertexShader;
        public static Shader BasicColorVertexShader
        {
            get
            {
                if (_basicColorVertexShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/BasicColorVertex/BasicColorVertexShader.vert"),
                        frag = new FragmentShaderObject("./Shaders/BasicColorVertex/BasicColorVertexShader.frag"))
                    {
                        _basicColorVertexShader = new Shader(vert, frag);
                    }
                    return _basicColorVertexShader;
                }
                return _basicColorVertexShader;
            }
        }

        private static Shader _lightShader;
        public static Shader LightShader
        {
            get
            {
                if (_lightShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/Lightning/Light/LightShader.vert"),
                        frag = new FragmentShaderObject("./Shaders/Lightning/Light/LightShader.frag"))
                    {
                        _lightShader = new Shader(vert, frag);
                    }
                    return _lightShader;
                }
                return _lightShader;
            }
        }

        private static Shader _litObjectShader;
        public static Shader LitObjectShader
        {
            get
            {
                if (_litObjectShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/Lightning/Object/LightObjectShader.vert"),
                        frag = new FragmentShaderObject("./Shaders/Lightning/Object/LightObjectShader.frag"))
                    {
                        _litObjectShader = new Shader(vert, frag);
                    }
                    return _litObjectShader;
                }
                return _litObjectShader;
            }
        }

        private static Shader _litTexturedObjectShader;
        public static Shader LitTexturedObjectShader
        {
            get
            {
                if (_litTexturedObjectShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/Lightning/TexturedObject/LightTexturedObjectShader.vert"),
                        frag = new FragmentShaderObject("./Shaders/Lightning/TexturedObject/LightTexturedObjectShader.frag"))
                    {
                        _litTexturedObjectShader = new Shader(vert, frag);
                    }
                    return _litTexturedObjectShader;
                }
                return _litTexturedObjectShader;
            }
        }

        private static Shader _basicPBRShader;
        public static Shader BasicPBRShader
        {
            get
            {
                if (_basicPBRShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/PBR/Basic/BasicPBR.vert"),
                        frag = new FragmentShaderObject("./Shaders/PBR/Basic/BasicPBR.frag"))
                    {
                        _basicPBRShader = new Shader(vert, frag);
                    }
                    return _basicPBRShader;
                }
                return _basicPBRShader;
            }
        }

        private static Shader _texturePBRShader;
        public static Shader TexturePBRShader
        {
            get
            {
                if (_texturePBRShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/PBR/Texture/TexturePBR.vert"),
                        frag = new FragmentShaderObject("./Shaders/PBR/Texture/TexturePBR.frag"))
                    {
                        _texturePBRShader = new Shader(vert, frag);
                    }
                    return _texturePBRShader;
                }
                return _texturePBRShader;
            }
        }

        private static Shader _polyChainShader;
        public static Shader PolyChainShader
        {
            get
            {
                if (_polyChainShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/PolyChain/PolyChainShader.vert"),
                        frag = new FragmentShaderObject("./Shaders/PolyChain/PolyChainShader.frag"),
                        geom = new GeometricShaderObject("./Shaders/PolyChain/PolyChainShader.frag"))
                    {
                        _polyChainShader = new Shader(vert, frag, geom);
                    }
                    return _polyChainShader;
                }
                return _polyChainShader;
            }
        }
    }
}
