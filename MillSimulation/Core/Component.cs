﻿using MillSimulation.Utils.UIExtensionClasses;

namespace MillSimulation.Core
{
    public abstract class Component : BindableObject
    {
        public SceneObject Owner;

        protected Component()
        {
        }

        public virtual void Dispose()
        {
        }
    }
}
