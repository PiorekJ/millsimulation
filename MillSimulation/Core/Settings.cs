﻿using MillSimulation.Utils.UIExtensionClasses;

namespace MillSimulation.Core
{
    public class Settings : BindableObject
    {
        private float _millingSpeed = 500.0f;

        public float MillingSpeed
        {
            get { return _millingSpeed; }
            set
            {
                _millingSpeed = value; 
                RaisePropertyChanged();
            }
        }
    }
}
