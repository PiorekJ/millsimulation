﻿using System.Windows.Media;
using MillSimulation.OpenTK;

namespace MillSimulation.Core
{
    public interface IColorable
    {
        Color Color { get; set; }
    }

    public abstract class Model : SceneObject
    {

        public IMesh Mesh;
        public Shader Shader;
        public ITexture Texture;
        protected Model()
        {
        }

        public override void Dispose()
        {
            Mesh?.Dispose();
        }

        protected virtual void OnRender()
        {

        }

        public void Render()
        {
            if (!IsActive)
                return;

            OnRender();
        }
    }
}
