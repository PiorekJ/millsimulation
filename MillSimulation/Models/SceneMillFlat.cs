﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using MillSimulation.Core;
using MillSimulation.OpenTK;
using MillSimulation.Utils.OpenTK;
using MillSimulation.Utils.Storage;
using OpenTK;
using PureCAD.Utils;

namespace MillSimulation.Models
{
    public class SceneMillFlat : SceneMill
    {
        public SceneMillFlat()
        {
            Shader = Shaders.LitObjectShader;
            Mesh = MeshGenerator.GenerateCylinderMesh();
        }

        public override void SetupMill(float sceneSize, int materialSize)
        {
            Mesh = MeshGenerator.GenerateCylinderMesh(sceneSize);
        }

        public override bool MillMaterialAt(SceneMaterial material, SceneMillPath.MaterialPoint materialPoint, float safeY, Vector3 millDirection, ref Vector2[] xIndieces, ref Vector2 zIndieces, bool updateMaterial)
        {
            for (int z = (int)(materialPoint.Position.Y - materialPoint.Radius);
                z <= (int)(materialPoint.Position.Y + materialPoint.Radius);
                z++)
            {
                var startIdx = -1;
                var endIdx = -1;

                if (z < 0 || z >= material.PointsZ)
                    continue;

                for (int x = (int)(materialPoint.Position.X - materialPoint.Radius);
                    x <= (int)(materialPoint.Position.X + materialPoint.Radius);
                    x++)
                {

                    if (x < 0 || x >= material.PointsX)
                        continue;

                    int localX = (int)(x - materialPoint.Position.X);
                    int localZ = (int)(z - materialPoint.Position.Y);
                    var idx = x + z * material.PointsX;

                    if (idx < material.VerticesLength && idx >= 0)
                    {
                        if (localX * localX + localZ * localZ <= materialPoint.RadiusSquared)
                        {
                            if (startIdx == -1)
                            {
                                if (zIndieces.X > z)
                                    zIndieces.X = z;
                                xIndieces[z].X = x;
                                startIdx = idx;
                            }
                            endIdx = idx;
                            xIndieces[z].Y = x;
                            if (zIndieces.Y < z)
                                zIndieces.Y = z;
                            var millResult = material.MillMaterialAt(idx, Transform.Position.Y, updateMaterial);

                            if (!millResult)
                            {
                                MessageBox.Show($"Milling below safe height, safe: {material.SafeY}, milled: {Transform.Position.Y}", "Error", MessageBoxButton.OK,
                                    MessageBoxImage.Error);
                                return false;
                            }
                            else if (millDirection.Y < 0)
                            {
                                MessageBox.Show($"Milling with flat end, mill direction is {millDirection}", "Error", MessageBoxButton.OK,
                                    MessageBoxImage.Error);
                                return false;
                            }

                        }
                    }
                }

                if (updateMaterial && startIdx != -1 && endIdx != -1)
                {
                    material.UpdateVertexStrip(startIdx, endIdx);
                }
            }
            return true;
        }

        protected override void OnRender()
        {
            Shader.Use();
            Shader.Bind(Shader.GetUniformLocation("model"), Transform.GetModelMatrix());
            Shader.Bind(Shader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
            Shader.Bind(Shader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());
            Shader.Bind(Shader.GetUniformLocation("color"), Color.ColorToVector3());
            Shader.Bind(Shader.GetUniformLocation("alpha"), 1.0f);
            Shader.Bind(Shader.GetUniformLocation("lightColor"), Simulation.Scene.SceneLights[0].Color.ColorToVector3());
            Shader.Bind(Shader.GetUniformLocation("lightPos"), Simulation.Scene.SceneLights[0].Transform.Position);
            Shader.Bind(Shader.GetUniformLocation("camPos"), Simulation.Scene.Camera.Transform.Position);
            Shader.Bind(Shader.GetUniformLocation("ks"), 0.3f);
            Shader.Bind(Shader.GetUniformLocation("kd"), 2f);
            Shader.Bind(Shader.GetUniformLocation("ka"), 0.5f);
            Shader.Bind(Shader.GetUniformLocation("m"), 100.0f);
            Mesh.Draw();
        }
    }
}
