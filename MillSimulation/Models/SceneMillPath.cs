﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using System.Windows.Media;
using MillSimulation.Core;
using MillSimulation.OpenTK;
using MillSimulation.Utils;
using MillSimulation.Utils.Math;
using MillSimulation.Utils.Storage;
using MillSimulation.Utils.UIExtensionClasses;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using PureCAD.Utils;

namespace MillSimulation.Models
{
    public class SceneMillPath : Model, IColorable
    {
        private List<ScenePath> _scenePaths;
        private int _currentSceneIndex;
        private MillType _millType;
        private float _millSize;
        private string _filename;
        public Color Color { get; set; } = Colors.LimeGreen;
        private bool _isSelected = true;

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                _isSelected = value;
                RaisePropertyChanged();
            }
        }

        public string Filename
        {
            get { return _filename; }
            set
            {
                _filename = value;
                RaisePropertyChanged();
            }
        }

        public MillType MillType
        {
            get { return _millType; }
            set
            {
                _millType = value;
                RaisePropertyChanged();
            }
        }

        public float MillSize
        {
            get { return _millSize; }
            set
            {
                _millSize = value;
                RaisePropertyChanged();
            }
        }

        public List<ScenePath> ScenePaths
        {
            get { return _scenePaths; }
            set
            {
                _scenePaths = value;
                RaisePropertyChanged();
            }
        }

        public int CurrentSceneIndex
        {
            get { return _currentSceneIndex; }
            set
            {
                _currentSceneIndex = value;
                RaisePropertyChanged();
            }
        }

        public int Length
        {
            get { return ScenePaths.Count - 1; }
        }

        public ICommand DeleteCommand { get; set; }

        public SceneMillPath(string filename, MillType type, float size, List<Vector3> sceneNodes)
        {
            Shader = Shaders.BasicColorVertexShader;
            Mesh = GeneratePathMesh(sceneNodes);
            ScenePaths = CreateScenePaths(sceneNodes);
            DeleteCommand = new RelayCommand(param => Delete());
            MillType = type;
            MillSize = size;
            Filename = filename;
            IsActive = false;
        }

        private Mesh<VertexPC> GeneratePathMesh(List<Vector3> sceneNodes)
        {
            var list = new List<VertexPC>();
            var edges = new List<uint>();
            for (var i = 0; i < sceneNodes.Count; i++)
            {
                Vector3 node = sceneNodes[i];
                list.Add(new VertexPC(node, Color.ColorToVector3()));
                if (i < sceneNodes.Count - 1)
                {
                    edges.Add((uint)i);
                    edges.Add((uint)i + 1);
                }
            }

            return new Mesh<VertexPC>(list, edges, MeshType.Lines, AccessType.Static);
        }

        protected override void OnRender()
        {
            GL.LineWidth(2);
            Shader.Use();
            Shader.Bind(Shader.GetUniformLocation("model"), Transform.GetModelMatrix());
            Shader.Bind(Shader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
            Shader.Bind(Shader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());
            Mesh.Draw();
            GL.LineWidth(1);
        }

        private List<ScenePath> CreateScenePaths(List<Vector3> sceneNodes)
        {
            var list = new List<ScenePath>();
            for (int i = 0; i < sceneNodes.Count - 1; i++)
            {
                list.Add(new ScenePath(sceneNodes[i], sceneNodes[i + 1]));
            }
            return list;
        }

        public void SetupScenePaths(SceneMaterial material, float millSize)
        {
            var localR = material.ConvertRadiusToModelSpace(millSize);
            var localR2 = localR * localR;
            for (var i = 0; i < ScenePaths.Count; i++)
            {
                ScenePath scenePath = ScenePaths[i];
                scenePath.SetupMaterialPoints(material, localR, localR2);
            }
        }

        private void Delete()
        {
            Simulation.Scene.RemoveSceneObject(this);
            Simulation.UIManager.DeletePath(this);
        }

        public class ScenePath : BindableObject
        {
            public Vector3 Start;
            public Vector3 End;
            public Vector3 MoveDirection;
            public List<MaterialPoint> MaterialPoints;

            public float TravelDistance;
            private int _currentMaterialIndex;
            public int CurrentMaterialIndex
            {
                get { return _currentMaterialIndex; }
                set
                {
                    _currentMaterialIndex = value;
                    RaisePropertyChanged();
                }
            }

            public ScenePath(Vector3 start, Vector3 end)
            {
                Start = start;
                End = end;
                MoveDirection = (end - start).Normalized();
                MaterialPoints = new List<MaterialPoint>();
            }

            public void SetupMaterialPoints(SceneMaterial material, int radius, int radiusSquared)
            {
                MaterialPoints.Clear();
                var localA = material.ConvertPointToModelSpace(Start);
                var localB = material.ConvertPointToModelSpace(End);

                //if (localA == localB)
                //{
                //    var len = (Start - End).Length;
                //    var count = len * material.GetVerticalStep();
                //    for (int i = 0; i < count; i++)
                //    {
                //        MaterialPoints.Add(new MaterialPoint(localA, material.ConvertPointToSceneSpace(localA), radius, radiusSquared));
                //    }
                //    TravelDistance = 1.0f / count;
                //    return;
                //}

                var materialPos = MathGeneralUtils.Bresenham2D((int)localA.X, (int)localA.Y,
                    (int)localB.X, (int)localB.Y);


                foreach (Vector2 pos in materialPos)
                {
                    MaterialPoints.Add(new MaterialPoint(pos, material.ConvertPointToSceneSpace(pos), radius, radiusSquared));
                }

                TravelDistance = 1.0f / (MaterialPoints.Count - 1);
            }
        }

        public class MaterialPoint
        {
            public Vector2 Position;
            public Vector3 ScenePosition;
            public int Radius;
            public int RadiusSquared;
            public int Diameter => 2 * Radius + 1;
            public MaterialPoint(Vector2 position, Vector3 scenePosition, int radius, int radiusSquared)
            {
                Position = position;
                ScenePosition = scenePosition;
                Radius = radius;
                RadiusSquared = radiusSquared;
            }
        }


    }
}
