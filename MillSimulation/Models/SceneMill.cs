﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using MillSimulation.Core;
using MillSimulation.OpenTK;
using MillSimulation.Utils;
using MillSimulation.Utils.OpenTK;
using MillSimulation.Utils.Storage;
using OpenTK;
using PureCAD.Utils;

namespace MillSimulation.Models
{


    public abstract class SceneMill : Model
    {
        public Color Color { get; set; } = Colors.Gray;
        public bool IsMoving { get; set; } = false;

        public SceneMill()
        {
            Shader = Shaders.LitObjectShader;
            Mesh = MeshGenerator.GenerateCubeMesh(1, 1, 1);
            Transform.Position = Constants.SafeMillPosition;
        }

        public abstract void SetupMill(float sceneSize, int materialSize);
        public abstract bool MillMaterialAt(SceneMaterial material, SceneMillPath.MaterialPoint materialPoint, float safeY, Vector3 millDirection, ref Vector2[] xIndieces, ref Vector2 zIndieces, bool updateMaterial = true);
    }
}

