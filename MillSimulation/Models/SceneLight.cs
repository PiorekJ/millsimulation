﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using MillSimulation.Core;
using MillSimulation.OpenTK;
using MillSimulation.Utils.OpenTK;
using OpenTK;
using PureCAD.Utils;

namespace MillSimulation.Models
{
    public class SceneLight : Model
    {
        public Color Color { get; set; } = Colors.White;
        public int LightIntensity = 500;
        public SceneLight()
        {
            Shader = Shaders.LightShader;
            Mesh = MeshGenerator.GenerateCubeMesh(0.25f, 0.25f, 0.25f);
        }

        protected override void OnRender()
        {
            Shader.Use();
            Shader.Bind(Shader.GetUniformLocation("model"), Transform.GetModelMatrix());
            Shader.Bind(Shader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
            Shader.Bind(Shader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());
            Shader.Bind(Shader.GetUniformLocation("color"), Color.ColorToVector3());
            Mesh.Draw();
        }
    }
}
