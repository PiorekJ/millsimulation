﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using MillSimulation.Core;
using MillSimulation.Utils;
using MillSimulation.Utils.Math;
using MillSimulation.Utils.Storage;
using OpenTK;
using PureCAD.Utils;

namespace MillSimulation.Models
{
    public class SceneMillingMachine : SceneObject
    {
        private SceneMill _mill;
        private SceneMaterial _material;
        private ObservableCollection<SceneMillPath> _paths;

        private SceneMillPath _currentSceneMillPath;
        private bool _isRunning;

        private float _moveTime;
        private int _millingSpeed = 10;

        public ObservableCollection<SceneMillPath> Paths
        {
            get { return _paths; }
            set
            {
                _paths = value;
                RaisePropertyChanged();
            }
        }

        public bool IsRunning
        {
            get { return _isRunning; }
            set
            {
                _isRunning = value;
                RaisePropertyChanged();
            }
        }

        public int MillingSpeed
        {
            get { return _millingSpeed; }
            set
            {
                _millingSpeed = value;
                RaisePropertyChanged();
            }
        }

        private Vector2[] _xIndieces;
        private Vector2 _zIndieces;
        public SceneMillingMachine(SceneMaterial material)
        {
            Paths = new ObservableCollection<SceneMillPath>();
            _material = material;
            _xIndieces = new Vector2[_material.PointsX];
            for (int i = 0; i < _xIndieces.Length; i++)
            {
                _xIndieces[i] = new Vector2(-1, -1);
            }
            _zIndieces = new Vector2(-1, -1);
        }

        public void AddPath(SceneMillPath path)
        {
            Paths.Add(path);
        }

        public void ClearPaths()
        {
            Paths.Clear();
            _currentSceneMillPath = null;
        }

        private void SetupMill(MillType type, float size, bool isActive = true)
        {
            if (_mill != null)
                _mill.IsActive = false;
            switch (type)
            {
                case MillType.Flat:
                    _mill = Simulation.Scene.SceneMillFlat;
                    break;
                case MillType.Sphere:
                    _mill = Simulation.Scene.SceneMillSphere;
                    break;
                default:
                    MessageBox.Show("Couldn't setup the mill, check file", "Error", MessageBoxButton.OK,
                        MessageBoxImage.Error);
                    return;
            }
            _mill.IsActive = isActive;
            _mill.SetupMill(size, _material.ConvertRadiusToModelSpace(size));
        }

        public void StartMilling(SceneMillPath path, bool startSimulation = true)
        {
            SetupMill(path.MillType, path.MillSize, startSimulation);
            _currentSceneMillPath = path;
            IsRunning = startSimulation;
        }

        public void MillInstant(SceneMillPath path)
        {
            StartMilling(path, false);

            while (path.CurrentSceneIndex < path.ScenePaths.Count)
            {
                if (path.ScenePaths[path.CurrentSceneIndex].CurrentMaterialIndex <
                    path.ScenePaths[path.CurrentSceneIndex].MaterialPoints.Count)
                {
                    _mill.Transform.Position = Vector3.Lerp(path.ScenePaths[path.CurrentSceneIndex].Start,
                        path.ScenePaths[path.CurrentSceneIndex].End,
                        path.ScenePaths[path.CurrentSceneIndex].CurrentMaterialIndex * path.ScenePaths[path.CurrentSceneIndex].TravelDistance);
                    var materialPoint = path.ScenePaths[path.CurrentSceneIndex].MaterialPoints[path.ScenePaths[path.CurrentSceneIndex].CurrentMaterialIndex];

                    if (!_mill.MillMaterialAt(_material, materialPoint, _material.SafeY,
                        path.ScenePaths[path.CurrentSceneIndex].MoveDirection, ref _xIndieces, ref _zIndieces, false))
                    {
                        IsRunning = false;
                        return;
                    }
                    path.ScenePaths[path.CurrentSceneIndex].CurrentMaterialIndex++;
                }
                else
                {
                    path.CurrentSceneIndex++;
                }
            }
            _material.UpdateAllVerticse();
        }

        private float _defaultFrameRate = 1 / 10f;
        private float _physTime = 1 / 10f;
        private float _timer = 0.0f;
        protected override void OnUpdate()
        {
            if (!IsRunning || _currentSceneMillPath == null)
                return;

            //_physTime = _defaultFrameRate + _timer;
            _zIndieces = new Vector2(_material.PointsZ, 0);
            for (int i = 0; i < MillingSpeed; i++)
            {
                if (_currentSceneMillPath.CurrentSceneIndex < _currentSceneMillPath.ScenePaths.Count)
                {
                    if (_currentSceneMillPath.ScenePaths[_currentSceneMillPath.CurrentSceneIndex].CurrentMaterialIndex <
                        _currentSceneMillPath.ScenePaths[_currentSceneMillPath.CurrentSceneIndex].MaterialPoints.Count)
                    {
                        _mill.Transform.Position = Vector3.Lerp(_currentSceneMillPath.ScenePaths[_currentSceneMillPath.CurrentSceneIndex].Start,
                            _currentSceneMillPath.ScenePaths[_currentSceneMillPath.CurrentSceneIndex].End,
                            _currentSceneMillPath.ScenePaths[_currentSceneMillPath.CurrentSceneIndex].CurrentMaterialIndex * _currentSceneMillPath.ScenePaths[_currentSceneMillPath.CurrentSceneIndex].TravelDistance);
                        var materialPoint = _currentSceneMillPath.ScenePaths[_currentSceneMillPath.CurrentSceneIndex].MaterialPoints[_currentSceneMillPath.ScenePaths[_currentSceneMillPath.CurrentSceneIndex].CurrentMaterialIndex];
                        if (!_mill.MillMaterialAt(_material, materialPoint, _material.SafeY,
                            _currentSceneMillPath.ScenePaths[_currentSceneMillPath.CurrentSceneIndex].MoveDirection, ref _xIndieces, ref _zIndieces, false))
                        {
                            IsRunning = false;
                            return;
                        }
                        _currentSceneMillPath.ScenePaths[_currentSceneMillPath.CurrentSceneIndex].CurrentMaterialIndex++;
                    }
                    else
                    {
                        _currentSceneMillPath.CurrentSceneIndex++;
                    }
                }
                else
                {
                    _paths.Remove(_currentSceneMillPath);
                    if (_paths.Count >= 1)
                        StartMilling(_paths[0]);
                }

                //_timer += Simulation.DeltaTime;
                //if (_physTime - _timer <= 0)
                //{
                //    _timer -= _physTime;
                //    break;
                //}
            }

            for (int i = (int)_zIndieces.X; i <= (int)_zIndieces.Y; i++)
            {
                for (int j = (int)_xIndieces[i].X; j <= (int)_xIndieces[i].Y; j++)
                {
                    _material.CalculateNormalAt(j + i * _material.PointsX);
                }
                _material.UpdateVertexStrip((int)_xIndieces[i].X + (int) _zIndieces.X *_material.PointsX , (int)_xIndieces[i].Y + (int)_zIndieces.Y * _material.PointsX);
            }

        }

        public void DeletePath(SceneMillPath path)
        {
            if (IsRunning && path == _currentSceneMillPath)
            {
                _currentSceneMillPath = null;
                IsRunning = false;
            }
            Paths.Remove(path);
        }
    }
}
