﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using MillSimulation.Components;
using MillSimulation.Core;
using MillSimulation.OpenTK;
using MillSimulation.Utils.OpenTK;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using PureCAD.Utils;

namespace MillSimulation.Models
{
    public class SceneSphere : Model
    {
        public Color Color { get; set; } = Colors.Red;

        public SceneSphere()
        {
            Shader = Shaders.TexturePBRShader;
            Mesh = MeshGenerator.GenerateSphereMeshStrip(1, 128, 128);
            Texture = Textures.CopperRockPBRTexture;           
        }

        protected override void OnRender()
        {
            Shader.Use();
            Shader.Bind(Shader.GetUniformLocation("model"), Transform.GetModelMatrix());
            Shader.Bind(Shader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
            Shader.Bind(Shader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());


            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, ((PBRTexture)Texture).Albedo.TexID);
            Shader.Bind(Shader.GetUniformLocation("albedoMap"), 0);
            GL.ActiveTexture(TextureUnit.Texture1);
            GL.BindTexture(TextureTarget.Texture2D, ((PBRTexture)Texture).Normal.TexID);
            Shader.Bind(Shader.GetUniformLocation("normalMap"), 1);
            GL.ActiveTexture(TextureUnit.Texture2);
            GL.BindTexture(TextureTarget.Texture2D, ((PBRTexture)Texture).Metallic.TexID);
            Shader.Bind(Shader.GetUniformLocation("metallicMap"), 2);
            GL.ActiveTexture(TextureUnit.Texture3);
            GL.BindTexture(TextureTarget.Texture2D, ((PBRTexture)Texture).Roughness.TexID);
            Shader.Bind(Shader.GetUniformLocation("roughnessMap"), 3);
            GL.ActiveTexture(TextureUnit.Texture4);
            GL.BindTexture(TextureTarget.Texture2D, ((PBRTexture)Texture).Ao.TexID);
            Shader.Bind(Shader.GetUniformLocation("aoMap"), 4);

            //Shader.Bind(Shader.GetUniformLocation("albedo"), Color.ColorToVector3());
            //Shader.Bind(Shader.GetUniformLocation("metallic"), 0.8f);
            //Shader.Bind(Shader.GetUniformLocation("roughness"), 0.2f);
            //Shader.Bind(Shader.GetUniformLocation("ao"), 1f);


            Shader.Bind(Shader.GetUniformLocation("camPos"), Simulation.Scene.Camera.Transform.Position);

            for (var i = 0; i < Simulation.Scene.SceneLights.Count; i++)
            {
              SceneLight sceneLight = Simulation.Scene.SceneLights[i];
              Shader.Bind(Shader.GetUniformLocation($"lightPositions[{i}]"), sceneLight.Transform.Position);
              Shader.Bind(Shader.GetUniformLocation($"lightColors[{i}]"), sceneLight.Color.ColorToVector3() * sceneLight.LightIntensity);
            }

           

            Mesh.Draw();

        }
    }
}
